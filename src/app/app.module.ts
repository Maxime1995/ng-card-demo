import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardListComponent } from './components/card-list/card-list/card-list.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { CardDetailComponent } from './components/card-detail/card-detail/card-detail.component';
import { NavbarComponent } from './components/navbar/navbar/navbar.component';
import { NgOptimizedImage } from "@angular/common";
import { UserRegisterComponent } from './components/user-register/user-register/user-register.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    CardListComponent,
    PaginationComponent,
    CardDetailComponent,
    NavbarComponent,
    UserRegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgOptimizedImage,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
