import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardListComponent } from './components/card-list/card-list/card-list.component';
import { CardDetailComponent } from './components/card-detail/card-detail/card-detail.component';
import { UserRegisterComponent } from "./components/user-register/user-register/user-register.component";

const routes: Routes = [
  { path: 'card-list', component: CardListComponent },
  { path: 'card-detail/:id', component: CardDetailComponent, pathMatch: 'full'},
  { path: 'user-register', component: UserRegisterComponent },
  { path: '**', redirectTo: 'card-list' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
